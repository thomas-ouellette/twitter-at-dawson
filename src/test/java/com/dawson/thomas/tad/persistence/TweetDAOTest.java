package com.dawson.thomas.tad.persistence;

import com.dawson.thomas.tad.data.TwitterInfo;
import com.dawson.thomas.tad.data.TwitterInfoDB;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import twitter4j.Logger;

/**
 *
 * @author Thomas
 */
public class TweetDAOTest {
    
    private static final Logger LOG = Logger.getLogger(TweetDAOTest.class);
    
    // Local mysql server config
    private static final String URL = "jdbc:mysql://localhost:3306/TADTweetDB?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";;
    private static final String USER = "TwitterClient";
    private static final String PWD = "password";
    
    private final TweetDAO tweetDAO;
    
    public TweetDAOTest(){
        tweetDAO = new TweetDAO();
    }
    
    @BeforeClass
    public static void printMsg(){
        LOG.info("== Testing TweetDAO methods ==");
    }
    
    @Before
    public void seedDB() {
        final String seedDataScript = loadAsString("createTweetTable.sql");
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }
    
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

    
    // TESTS
    
    
    
    /**
     * Test of saveTweet method, of class TweetDAO.
     */
    @Test
    public void testSaveTweet() {
        try {
            LOG.info("testing saveTweet with valid input");
            long id = 69420L;
            TwitterInfoDB twitterInfoDB = new TwitterInfoDB(id, "saved", "Thomas Ouellette", "tman375", "This is a test tweet", new Date());
            tweetDAO.saveTweet(twitterInfoDB);
            int numRecords = tweetDAO.getSavedTweets(1).size() + tweetDAO.getSavedTweets(2).size();
            int expectedNumRecords = 38;
            assertEquals(expectedNumRecords, numRecords);
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }
    
    /**
     * Test of saveTweet method, of class TweetDAO.
     */
    @Test
    public void testSaveTweetNull() {
        try {
            LOG.info("testing saveTweet with invalid input(null)");
            assertFalse(tweetDAO.saveTweet(null));
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }
    
    /**
     * Test of saveTweet method, of class TweetDAO.
     */
    @Test
    public void testSaveTweetEmptyTwitterInfoDB() {
        try {
            LOG.info("testing saveTweet with invalid input (empty TwitterInfoDB)");
            TwitterInfoDB twitterInfoDB = new TwitterInfoDB();
            assertFalse(tweetDAO.saveTweet(twitterInfoDB));
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }
    
    /**
     * Test of saveTweet method, of class TweetDAO.
     */
    @Test
    public void testSaveTweetInvalidTwitterInfoDB() {
        try {
            LOG.info("testing saveTweet with invalid input (invalid values in TwitterInfoDB)");
            TwitterInfoDB twitterInfoDB = new TwitterInfoDB(-23462346L, null, null, null, null, null);
            assertFalse(tweetDAO.saveTweet(twitterInfoDB));
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }

    /**
     * Test of getSavedTweets method, of class TweetDAO.
     */
    @Test
    public void testGetSavedTweets() {
        LOG.info("testing getSavedTweets, checking number of records returned");
        int numRecords = 0;
        try {
            numRecords = tweetDAO.getSavedTweets(1).size() + tweetDAO.getSavedTweets(2).size();
        } catch (SQLException | IOException ex) {
            LOG.error("Couldn't retrieve all records", ex);
            fail("Couldn't retrieve all records.");
        }
        int expectedNumRecords = 37;
        assertEquals(expectedNumRecords, numRecords);
    }

    /**
     * Test of isSaved method, of class TweetDAO.
     */
    @Test
    public void testIsSaved() {
        try {
            LOG.info("testing isSaved with valid input");
            long id = 69420L;
            TwitterInfoDB twitterInfoDB = new TwitterInfoDB(id, "saved", "Thomas Ouellette", "tman375", "This is a test tweet", new Date());
            tweetDAO.saveTweet(twitterInfoDB);
            assertTrue(tweetDAO.isSaved(id));
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }
    
    /**
     * Test of isSaved method, of class TweetDAO.
     */
    @Test
    public void testIsSavedFalse() {
        try {
            LOG.info("testing isSaved with id that doesn't exist");
            long id = 69420L;
            assertFalse(tweetDAO.isSaved(id));
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }

    /**
     * Test of unsaveTweet method, of class TweetDAO.
     */
    @Test
    public void testUnsaveTweet() {
        try {
            LOG.info("testing unaveTweet with valid input");
            long id = 1191498322177142784L;
            tweetDAO.unsaveTweet(id);
            int numRecords = tweetDAO.getSavedTweets(1).size() + tweetDAO.getSavedTweets(2).size();
            int expectedNumRecords = 36;
            assertEquals(expectedNumRecords, numRecords);
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }
    
    /**
     * Test of unsaveTweet method, of class TweetDAO.
     */
    @Test
    public void testUnsaveTweetInvalid() {
        try {
            LOG.info("testing unaveTweet with non existent id");
            long id = 69420L;
            tweetDAO.unsaveTweet(id);
            int numRecords = tweetDAO.getSavedTweets(1).size() + tweetDAO.getSavedTweets(2).size();
            int expectedNumRecords = 37;
            assertEquals(expectedNumRecords, numRecords);
        } catch (SQLException | IOException ex) {
            LOG.error("Test shouldn't have thrown any exceptions...", ex);
            fail("Test threw an exception even though data provided was supposed to be valid.");
        }
    }
    
}
