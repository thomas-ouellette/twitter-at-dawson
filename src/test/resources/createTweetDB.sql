DROP DATABASE IF EXISTS TADTweetDB;
CREATE DATABASE TADTweetDB;

USE TADTweetDB;

DROP USER IF EXISTS 'TwitterClient'@'localhost';
CREATE USER 'TwitterClient'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON TADTweetDB.* TO 'TwitterClient'@'localhost';
FLUSH PRIVILEGES;

