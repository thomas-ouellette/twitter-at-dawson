package com.dawson.thomas.tad.persistence;

import com.dawson.thomas.tad.data.DatabaseProperties;
import com.dawson.thomas.tad.data.TwitterInfoDB;
import com.dawson.thomas.tad.manager.DatabasePropertiesManager;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used for saving tweets, retrieving saved tweets, and un-saving tweets.
 * In other words: a data access object that talks to the database.
 *
 * @author Thomas
 */
public class TweetDAO {

    private final static Logger LOG = LoggerFactory.getLogger(TweetDAO.class);

    private final DatabaseProperties dbProps;

    private final DatabasePropertiesManager propertiesManager;

    public TweetDAO() {
        propertiesManager = new DatabasePropertiesManager();
        dbProps = new DatabaseProperties();
    }

    /**
     * Method used to save a given tweet to the database from a user's timeline.
     *
     * @param twitterInfoDB
     * @return {@code true} on success, {@code false} otherwise.
     * @throws SQLException
     * @throws IOException
     */
    public boolean saveTweet(TwitterInfoDB twitterInfoDB) throws SQLException, IOException {
        if (validTwitterInfo(twitterInfoDB)) {
            try (Connection c = getConnection()) {
                String insert = "INSERT INTO tweets VALUES(?, ?, ?, ?, ?, ?);";
                PreparedStatement stmt = c.prepareStatement(insert);
                stmt.setLong(1, twitterInfoDB.getStatusId());
                stmt.setString(2, twitterInfoDB.getTweetCategory());
                stmt.setString(3, twitterInfoDB.getScreenName());
                stmt.setString(4, twitterInfoDB.getUsername());
                stmt.setString(5, twitterInfoDB.getTweetText());
                Timestamp timestamp = new Timestamp(twitterInfoDB.getCreatedAtDate().getTime());
                stmt.setTimestamp(6, timestamp);
                return stmt.execute();
            }
        }

        return false;
    }

    /**
     * Returns a paged list of saved tweets from the database.
     *
     * @param page which page to retrieve
     * @return A list of tweets, represented by the TwitterInfoDB object.
     * @throws SQLException
     * @throws IOException
     */
    public List<TwitterInfoDB> getSavedTweets(int page) throws SQLException, IOException {
        List<TwitterInfoDB> tweets = new ArrayList<>();
        try (Connection c = getConnection()) {

            int resultsPerPage = 20;

            PreparedStatement stmt;

            // If not getting page 1, use an offset
            if (page > 1) {
                stmt = c.prepareStatement("SELECT * FROM tweets ORDER BY created_at_date DESC LIMIT ?, ?;");
                int offset = (page - 1) * resultsPerPage;
                stmt.setInt(1, offset);
                stmt.setInt(2, resultsPerPage);
            } else {
                stmt = c.prepareStatement("SELECT * FROM tweets ORDER BY created_at_date DESC LIMIT ?;");
                stmt.setInt(1, resultsPerPage);
            }

            ResultSet results = stmt.executeQuery();
            while (results.next()) {
                tweets.add(createTwitterInfoDB(results));
            }
        }
        return tweets;
    }

    /**
     * Method used to check whether or not a given tweet has been saved 
     * into the database.
     * 
     * @param id the status id of the given tweet.
     * @return true if found, false otherwise.
     * @throws SQLException
     * @throws IOException 
     */
    public boolean isSaved(long id) throws SQLException, IOException {
        try (Connection c = getConnection()) {
            PreparedStatement stmt = c.prepareStatement("SELECT * FROM tweets WHERE status_id = ?;");
            stmt.setLong(1, id);
            ResultSet results = stmt.executeQuery();
            if (results.next()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Un-saves a tweet - deletes it from the database.
     *
     * @param id the id of the tweet to un-save
     * @return {@code true} on success, {@code false} otherwise.
     * @throws SQLException
     * @throws IOException
     */
    public boolean unsaveTweet(long id) throws SQLException, IOException {
        try (Connection c = getConnection()) {
            String delete = "DELETE FROM tweets WHERE status_id = ?;";
            PreparedStatement stmt = c.prepareStatement(delete);
            stmt.setLong(1, id);
            return stmt.execute();
        }
    }

    /**
     * Helper method used to get a connection to the database.
     *
     * @return a connection object pointing to the twitter db.
     * @throws SQLException
     * @throws IOException
     */
    private Connection getConnection() throws SQLException, IOException {
        if (propertiesManager.loadTextProperties(dbProps, "./src/main/resources/", "database_properties")) {
            String url = dbProps.getDbURL() + ":" + dbProps.getDbPort() + "/" + dbProps.getDbName() + "?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
            return DriverManager.getConnection(url, dbProps.getDbUsername(), dbProps.getDbPassword());
        }

        return null;
    }

    /**
     * Helper method used to create TwitterInfoDB beans from a results set.
     *
     * @param result
     * @return a row from the database (represented by the TwiiterInfoDB object)
     * @throws SQLException
     */
    private TwitterInfoDB createTwitterInfoDB(ResultSet result) throws SQLException {
        long id = result.getLong("status_id");
        String category = result.getString("tweet_category");
        String name = result.getString("screen_name");
        String handle = result.getString("username");
        String text = result.getString("tweet_text");
        Timestamp created = result.getTimestamp("created_at_date");

        return new TwitterInfoDB(id, category, name, handle, text, created);
    }

    
    /**
     * Validation method for method that saves tweets into the database.
     * 
     * @param twitterInfoDB the given data bean to validate.
     * @return true if the bean has valid data, false otherwise.
     */
    private boolean validTwitterInfo(TwitterInfoDB twitterInfoDB) {
        if (twitterInfoDB != null && twitterInfoDB.getStatusId() > 0L) {
            if (twitterInfoDB.getScreenName() != null && !twitterInfoDB.getScreenName().isBlank()) {
                if (twitterInfoDB.getUsername() != null && !twitterInfoDB.getUsername().isBlank()) {
                    if (twitterInfoDB.getTweetText() != null && !twitterInfoDB.getTweetText().isBlank()) {
                        Date created = twitterInfoDB.getCreatedAtDate();
                        // second condition makes sure the date isn't in the future
                        if (created != null && created.compareTo(new Date()) <= 0) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}
