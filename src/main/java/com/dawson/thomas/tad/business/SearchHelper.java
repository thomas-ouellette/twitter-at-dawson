/**
 * Classes for dealing with business-level logic of the application.
 * 
 * <p>
 * The principle responsibility of these classes is querying the Twitter4J 
 * library. The core class is {@link com.dawson.thomas.tad.business.TwitterEngine}
 * that enables you to
 * <ul>
 * <li> Retrieve the authenticated User's timeline
 * <li> Retrieve the authenticated User's retweets
 * <li> Retrieve the authenticated User's mentions
 * <li> Retrieve the authenticated User's messages
 * <li> Send new tweets and messages
 * <li> Search through all Tweets
 * <li> Page results that are displayed to the user
 * </ul>
 */
package com.dawson.thomas.tad.business;

import com.dawson.thomas.tad.data.TwitterInfo;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * Classes used for keeping track of paging for Twitter search results.
 * 
 * @author Thomas
 */
public class SearchHelper implements ITwitterPagingHelper {
    
    private final static Logger LOG = LoggerFactory.getLogger(SearchHelper.class);

    private final ObservableList<TwitterInfo> list;

    private final TwitterEngine twitterEngine;
    
    private final String query;
    
    public SearchHelper(ObservableList<TwitterInfo> list, String query){
        twitterEngine = new TwitterEngine();
        this.list = list;
        this.query = query;
    }
    
    @Override
    public void load() throws TwitterException {
        List<Status> newItems = twitterEngine.searchtweets(query);
        newItems.forEach(status -> list.add(list.size(), new TwitterInfo(status)));
    }
}
