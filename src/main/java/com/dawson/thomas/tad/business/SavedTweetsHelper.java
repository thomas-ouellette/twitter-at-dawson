/**
 * Classes for dealing with business-level logic of the application.
 * 
 * <p>
 * The principle responsibility of these classes is querying the Twitter4J 
 * library. The core class is {@link com.dawson.thomas.tad.business.TwitterEngine}
 * that enables you to
 * <ul>
 * <li> Retrieve the authenticated User's timeline
 * <li> Retrieve the authenticated User's retweets
 * <li> Retrieve the authenticated User's mentions
 * <li> Retrieve the authenticated User's messages
 * <li> Send new tweets and messages
 * <li> Search through all Tweets
 * <li> Page results that are displayed to the user
 * </ul>
 */
package com.dawson.thomas.tad.business;

import com.dawson.thomas.tad.data.TwitterInfoDB;
import com.dawson.thomas.tad.persistence.TweetDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classes used for keeping track of paging for database results.
 * 
 * @author Thomas
 */
public class SavedTweetsHelper {

    private final static Logger LOG = LoggerFactory.getLogger(SavedTweetsHelper.class);

    private final ObservableList<TwitterInfoDB> list;

    private int page;
    
    private final TweetDAO tweetDAO;
    
    public SavedTweetsHelper(ObservableList<TwitterInfoDB> list){
        page = 1;
        this.list = list;
        tweetDAO = new TweetDAO();
    }
    
    public void load() throws SQLException, IOException {
        List<TwitterInfoDB> savedTweets = tweetDAO.getSavedTweets(page);
        savedTweets.forEach(savedTweet -> list.add(savedTweet));
        page++;
    }
    
}
