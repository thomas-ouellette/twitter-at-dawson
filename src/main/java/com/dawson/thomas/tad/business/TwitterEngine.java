/**
 * Classes for dealing with business-level logic of the application.
 * 
 * <p>
 * The principle responsibility of these classes is querying the Twitter4J 
 * library. The core class is {@link com.dawson.thomas.tad.business.TwitterEngine}
 * that enables you to
 * <ul>
 * <li> Retrieve the authenticated User's timeline
 * <li> Retrieve the authenticated User's retweets
 * <li> Retrieve the authenticated User's mentions
 * <li> Retrieve the authenticated User's messages
 * <li> Send new tweets and messages
 * <li> Search through all Tweets
 * <li> Page results that are displayed to the user
 * </ul>
 */
package com.dawson.thomas.tad.business;

import com.dawson.thomas.tad.data.MessageBean;
import com.dawson.thomas.tad.data.UserBean;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Business class with methods used for interaction with the Twitter4J library.
 *
 * @author Thomas
 */
public class TwitterEngine {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterEngine.class);

    /**
     * Retrieves an instance of a Twitter object. Method essentially an 
     * alias for TwitterFactory.getSingleton().
     * 
     * Method used by all method in this class to interact with Twitter 
     * via the Twitter object.
     * 
     * @return a Twitter singleton object
     */
    public Twitter getTwitterInstance() {
        return TwitterFactory.getSingleton();
    }

    /**
     * Retrieves the authenticated user's Twitter timeline - the main feed 
     * of tweets from users that the authenticated user follows. Retrieves 
     * 50 tweets at a time, results are returned page by page.
     * 
     * @param page which page of the user's timeline to retrieve. 
     * @return a list of Status objects - each one representing a single tweet.
     * @throws TwitterException 
     */
    public List<Status> getTimeline(int page) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Paging paging = new Paging();
        paging.setCount(50);
        paging.setPage(page);
        List<Status> statuses = twitter.getHomeTimeline(paging);
        return statuses;
    }

    /**
     * Sends a tweet with only text
     *
     * @param tweet the text content of the new tweet.
     * @throws TwitterException
     */
    public void createTweet(String tweet) throws TwitterException {
        LOG.debug("createTweet: " + tweet);
        Twitter twitter = getTwitterInstance();
        Status status = twitter.updateStatus(tweet);
    }
    
    /**
     * Method used for send direct messages to other users. It takes a 
     * message text and a user's id as input.
     * 
     * @param text message text to send.
     * @param recipient user who will receive the message.
     * @throws TwitterException 
     */
    public void sendMessage(String text, long recipient) throws TwitterException{
        getTwitterInstance().sendDirectMessage(recipient, text);
    }

    /**
     * Retrieves the authenticated user's "mentions timeline" - all tweets 
     * that @ mention the user by their handle. The results are retrieved 
     * 50 tweets at a time, page by page.
     * 
     * @param page which page of the user's mentions timeline to retrieve.
     * @return a list of Status objects - each one representing a single tweet.
     * @throws TwitterException 
     */
    public List<Status> getMentions(int page) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Paging paging = new Paging();
        paging.setCount(50);
        paging.setPage(page);
        List<Status> statuses = twitter.getMentionsTimeline(paging);
        return statuses;
    }

    /**
     * Retrieves the authenticated user's retweets - all tweets 
     * that the user has retweeted. The results are retrieved 
     * 50 tweets at a time, page by page.
     * 
     * @param page which page of the user's mentions timeline to retrieve.
     * @return a list of Status objects - each one representing a single tweet.
     * @throws TwitterException 
     */
    public List<Status> getMyRetweets(int page) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Paging paging = new Paging();
        paging.setCount(50);
        paging.setPage(page);
        List<Status> myRetweets = twitter.getHomeTimeline(paging).stream()
                .filter(Status::isRetweetedByMe)
                .collect(Collectors.toList());
        LOG.info("MY RETWEETS: " + myRetweets);
        myRetweets.stream()
                .forEach(status -> LOG.info("RETWEET: " + status.toString()));
        return myRetweets;
    }

    /**
     * Retrieves the authenticated user's tweets that have been retweeted 
     * by others. The results are retrieved 50 tweets at a time, page by page.
     * 
     * @param page which page of the user's mentions timeline to retrieve.
     * @return a list of Status objects - each one representing a single tweet.
     * @throws TwitterException 
     */
    public List<Status> getMyTweetsRetweeted(int page) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Paging paging = new Paging();
        paging.setCount(50);
        paging.setPage(page);
        List<Status> myRetweets = twitter.getRetweetsOfMe(paging);
        return myRetweets;
    }

    /**
     * Search for tweets whose text contains the searchTerm provided 
     * to the method.
     *
     * @param searchTerm the term to look for in the text content of all tweets.
     * @return a list of Status objects - each one representing a single tweet.
     * @throws TwitterException
     */
    public List<Status> searchtweets(String searchTerm) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Query query = new Query(searchTerm);
        QueryResult result = twitter.search(query);
        List<Status> statuses = result.getTweets();
        return statuses;
    }
    
    /**
     * Marks a given tweet as favorited.
     * 
     * @param statusId the id of the tweet to favorite.
     * @throws TwitterException 
     */
    public void createFavorite(long statusId) throws TwitterException{
        getTwitterInstance().createFavorite(statusId);
    }
    
    /**
     * Unfavorites a favorited tweet.
     * 
     * @param statusId the id of the tweet to unfavorite.
     * @throws TwitterException 
     */
    public void destroyFavorite(long statusId) throws TwitterException{
        getTwitterInstance().destroyFavorite(statusId);
    }
    
    /**
     * Retweets a given tweet.
     * 
     * @param statusId the id of the tweet to retweet.
     * @throws TwitterException 
     */
    public void createRetweet(long statusId) throws TwitterException{
        getTwitterInstance().retweetStatus(statusId);
    }
    
    /**
     * Un-retweets a given tweet.
     * 
     * @param statusId the id of the tweet to Un-retweet.
     * @throws TwitterException 
     */
    public void removeRetweet(long statusId) throws TwitterException{
        getTwitterInstance().unRetweetStatus(statusId);
    }
    
    /**
     * Retweets a given tweet and adds a comment to it.
     * 
     * @param statusId the id of the tweet to retweet.
     * @param comment the text comment to add to the retweet.
     * @throws TwitterException 
     */
    public void createRetweetWithComment(long statusId, String comment) throws TwitterException{
        StatusUpdate status = new StatusUpdate(comment);
        status.setInReplyToStatusId(statusId);
        getTwitterInstance().updateStatus(status);
    }
    
    /**
     * Retrieves a list of users who have messaged the authenticated user.
     * For each user, it creates one UserBean object which holds a few pieces 
     * of information meant for display on the UI.
     * 
     * @return a list of UserBeans representing users who have previously 
     *         direct messaged the authenticated user. 
     * @throws TwitterException 
     */
    public List<UserBean> getUsersWhoHaveMessagedMe() throws TwitterException{
        Twitter twitter = getTwitterInstance();
        String cursor = null;
        int count = 20;
        DirectMessageList messages;
        ArrayList<UserBean> userList = new ArrayList<>();
        ArrayList<Long> userIds = new ArrayList<>();
        do {
            messages = cursor == null ? twitter.getDirectMessages(count) : twitter.getDirectMessages(count, cursor);
            for (DirectMessage message : messages) {
                if(!userIds.contains(message.getSenderId())){
                    userIds.add(message.getSenderId());
                    userList.add(new UserBean(message.getSenderId(), getUsername(message.getSenderId()), message.getText()));
                }
            }
            cursor = messages.getNextCursor();
        } while (messages.size() > 0 && cursor != null);
        
        return userList;
    }
    
    /**
     * Retrieves the display name of a user. The method uses a user's 
     * id to find their name on Twitter.
     * 
     * @param userId the id of a given user.
     * @return a string representing a given user's display name.
     * @throws TwitterException 
     */
    public String getUsername(long userId) throws TwitterException{
        return getTwitterInstance().showUser(userId).getName();
    }
    
    /**
     * Method used to retrieve all the messages in a direct message 
     * conversation between the authenticated user and another user.
     * 
     * Each message is stored into a bean which is used for display purposes.
     * 
     * @param userId the id of a given user.
     * @return a list of MessageBeans representing direct messages.
     * @throws TwitterException 
     */
    public List<MessageBean> getConversation(long userId) throws TwitterException{
        Twitter twitter = getTwitterInstance();
        long myId = twitter.getId();
        String cursor = null;
        int count = 20;
        DirectMessageList messages;
        ArrayList<MessageBean> myMessages = new ArrayList<>();
        do {
            messages = cursor == null ? twitter.getDirectMessages(count) : twitter.getDirectMessages(count, cursor);
            for (DirectMessage message : messages) {
                if((message.getSenderId() == myId && message.getRecipientId() == userId)
                        || (message.getSenderId() == userId && message.getRecipientId() == myId)){
                    myMessages.add(new MessageBean(message.getSenderId(), message.getText()));
                }
            }
            cursor = messages.getNextCursor();
        } while (messages.size() > 0 && cursor != null);
        Collections.reverse(myMessages);
        return myMessages;
    }
}
