/**
 * Classes for dealing with business-level logic of the application.
 * 
 * <p>
 * The principle responsibility of these classes is querying the Twitter4J 
 * library. The core class is {@link com.dawson.thomas.tad.business.TwitterEngine}
 * that enables you to
 * <ul>
 * <li> Retrieve the authenticated User's timeline
 * <li> Retrieve the authenticated User's retweets
 * <li> Retrieve the authenticated User's mentions
 * <li> Retrieve the authenticated User's messages
 * <li> Send new tweets and messages
 * <li> Search through all Tweets
 * <li> Page results that are displayed to the user
 * </ul>
 */
package com.dawson.thomas.tad.business;

import com.dawson.thomas.tad.data.TwitterInfo;
import java.util.List;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * Classes used for keeping track of paging for retweets timeline.
 * 
 * @author Thomas
 */
public class MyRetweetsHelper implements ITwitterPagingHelper {
    
    private final static Logger LOG = LoggerFactory.getLogger(MyRetweetsHelper.class);

    private final ObservableList<TwitterInfo> list;

    private final TwitterEngine twitterEngine;

    private int page;
    
    public MyRetweetsHelper(ObservableList<TwitterInfo> list){
        page = 1;
        twitterEngine = new TwitterEngine();
        this.list = list;
    }
        
    @Override
    public void load() throws TwitterException {
        List<Status> newItems = twitterEngine.getMyRetweets(page);
        newItems.forEach(status -> list.add(list.size(), new TwitterInfo(status)));
        page++;
    }
}
