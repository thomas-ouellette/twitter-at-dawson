/**
 * Classes for dealing with business-level logic of the application.
 * 
 * <p>
 * The principle responsibility of these classes is querying the Twitter4J 
 * library. The core class is {@link com.dawson.thomas.tad.business.TwitterEngine}
 * that enables you to
 * <ul>
 * <li> Retrieve the authenticated User's timeline
 * <li> Retrieve the authenticated User's retweets
 * <li> Retrieve the authenticated User's mentions
 * <li> Retrieve the authenticated User's messages
 * <li> Send new tweets and messages
 * <li> Search through all Tweets
 * <li> Page results that are displayed to the user
 * </ul>
 */
package com.dawson.thomas.tad.business;

import twitter4j.TwitterException;

/**
 * This interface is used for all classes used for paging results from Twitter.
 * 
 * @author Thomas
 */
public interface ITwitterPagingHelper {
    
    /**
     * Method used to get the current page of results from Twitter.
     * 
     * @throws TwitterException 
     */
    public void load() throws TwitterException;
    
}
