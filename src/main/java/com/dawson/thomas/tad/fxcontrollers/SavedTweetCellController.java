package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.data.TwitterInfoDB;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class SavedTweetCellController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="tweet_cell"
    private HBox tweet_cell; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private Text name; // Value injected by FXMLLoader

    @FXML // fx:id="twitter_handle"
    private Text twitter_handle; // Value injected by FXMLLoader

    @FXML // fx:id="date"
    private Text date; // Value injected by FXMLLoader

    @FXML // fx:id="tweet_text"
    private Text tweet_text; // Value injected by FXMLLoader
    
    @FXML
    private Button delete_btn;

    private TwitterInfoDB twitterInfoDB;
    
    private SavedTweetsModuleController parentController;
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        delete_btn.setOnAction(e -> unsaveTweet());
    }
    
    public void setTwitterInfoDB(TwitterInfoDB twitterInfoDB){
        this.twitterInfoDB = twitterInfoDB;
        populateFields();
    }
    
    public void setParentController(SavedTweetsModuleController parentController){
        this.parentController = parentController;
    }
    
    private void populateFields(){
        if(twitterInfoDB != null){
            twitter_handle.setText("@"+twitterInfoDB.getUsername());
            String displayedDate = twitterInfoDB.getCreatedAtDate().toString();
            date.setText(displayedDate.substring(0, displayedDate.length()-5));
            tweet_text.setText(twitterInfoDB.getTweetText());
            name.setText(twitterInfoDB.getScreenName());
        }
    }
    
    private void unsaveTweet(){
        if(twitterInfoDB != null && parentController != null){
            parentController.unsaveTweet(twitterInfoDB.getStatusId());
        }
    }
}
