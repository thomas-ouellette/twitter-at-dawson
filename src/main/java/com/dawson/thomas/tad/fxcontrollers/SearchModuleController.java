/**
 * Package containing all UI controllers. Presentation layer classes 
 * which talk to/interact with business layer objects.
 * 
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.SearchHelper;
import com.dawson.thomas.tad.presentation.TweetCell;
import com.dawson.thomas.tad.data.TwitterInfo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import com.dawson.thomas.tad.business.ITwitterPagingHelper;

public class SearchModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(SearchModuleController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="search_bar"
    private TextField search_bar; // Value injected by FXMLLoader

    @FXML // fx:id="results"
    private ListView<TwitterInfo> results; // Value injected by FXMLLoader

    @FXML // fx:id="search_twitter"
    private Button search_twitter; // Value injected by FXMLLoader
    
    @FXML
    private Label error_display;

    private ITwitterPagingHelper twitterHelper;
    
    private String searchTerm;
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        setupListView();
        search_twitter.setOnAction(e -> searchTwitter());
        search_bar.setOnKeyTyped(e -> updateButton());
    }
    
    private void setupListView() {
        ObservableList<TwitterInfo> list = FXCollections.observableArrayList();
        results.setItems(list);
        results.setCellFactory(p -> new TweetCell());
    }
    
    private void searchTwitter(){
        searchTerm = search_bar.getText();
        if(searchTerm.isBlank()){
            return;
        } else {
            search_twitter.setDisable(true);
            
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        results.getItems().clear();
                        twitterHelper = new SearchHelper(results.getItems(), searchTerm);
                        twitterHelper.load();
                    } catch (TwitterException te) {
                        LOG.error("Something went wrong while searching.", te);
                        error_display.setText(resources.getString("error_search"));
                    }
                }
            };
            thread.start();
        }
    }
    
    private void updateButton(){
        if(search_twitter.isDisabled() && !search_bar.getText().isBlank()){
            search_twitter.setDisable(false);
        }
    }
}
