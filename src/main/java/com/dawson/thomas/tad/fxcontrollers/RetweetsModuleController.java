package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.MyRetweetsHelper;
import com.dawson.thomas.tad.business.MyTweetsRetweetedHelper;
import com.dawson.thomas.tad.presentation.TweetCell;
import com.dawson.thomas.tad.data.TwitterInfo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;
import com.dawson.thomas.tad.business.ITwitterPagingHelper;

public class RetweetsModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(RetweetsModuleController.class);

    @FXML // fx:id="retweets"
    private ListView<TwitterInfo> retweets; // Value injected by FXMLLoader

    @FXML // fx:id="retweets_by_you"
    private Button retweets_by_you; // Value injected by FXMLLoader

    @FXML // fx:id="your_tweets_retweeted"
    private Button your_tweets_retweeted; // Value injected by FXMLLoader

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="timeline"
    private ListView<?> timeline; // Value injected by FXMLLoader

    @FXML // fx:id="error_display"
    private Text error_display; // Value injected by FXMLLoader

    @FXML // fx:id="refresh_button"
    private Button refresh_button; // Value injected by FXMLLoader

    @FXML // fx:id="load_more_button"
    private Button load_more_button; // Value injected by FXMLLoader

    private final static String YOUR_TWEETS_RETWEETED = "your_tweets_retweeted";
    private final static String RETWEETS_BY_YOU = "retweets_by_you";
    private final static String STYLE_SELECTED = "-fx-background-color: lightgrey;";
    private final static String STYLE_DESELECTED = "-fx-background-color: 000;";

    private static String selected;

    private ITwitterPagingHelper twitterHelper;

    public RetweetsModuleController() {
        super();
        LOG.info("RetweetsModuleController constructor");
    }

    @FXML
    private void initialize() {
        LOG.info("initializing RetweetsModuleController");
        setupListView();
        displayYourRetweets();
        retweets_by_you.setOnAction(e -> displayYourRetweets());
        your_tweets_retweeted.setOnAction(e -> displayYourTweetsRetweeted());
        refresh_button.setOnAction(e -> refreshRetweets());
        load_more_button.setOnAction(e -> loadMoreRetweets());
    }

    private void setupListView() {
        ObservableList<TwitterInfo> list = FXCollections.observableArrayList();
        retweets.setItems(list);
        retweets.setCellFactory(p -> new TweetCell());
    }

    private void displayYourRetweets() {
        LOG.info("displayYourRetweets()");
        if (selected != null && selected.compareTo(RETWEETS_BY_YOU) == 0) {
            LOG.info("already selected");
            return;
        } else {
            LOG.info("changing selection");
            retweets_by_you.setStyle(STYLE_SELECTED);
            your_tweets_retweeted.setStyle(STYLE_DESELECTED);
            selected = RETWEETS_BY_YOU;
            refreshRetweets();
        }
    }

    private void displayYourTweetsRetweeted() {
        LOG.info("displayYourTweetsRetweeted()");
        if (selected.compareTo(YOUR_TWEETS_RETWEETED) == 0) {
            LOG.info("already selected");
            return;
        } else {
            LOG.info("changing selection");
            your_tweets_retweeted.setStyle(STYLE_SELECTED);
            retweets_by_you.setStyle(STYLE_DESELECTED);
            selected = YOUR_TWEETS_RETWEETED;
            refreshRetweets();
        }
    }

    private void refreshRetweets() {
        Thread thread = new Thread() {
            public void run() {
                try {
                    retweets.getItems().clear();
                    if (selected.compareTo(YOUR_TWEETS_RETWEETED) == 0) {
                        twitterHelper = new MyTweetsRetweetedHelper(retweets.getItems());
                        twitterHelper.load();
                    } else {
                        twitterHelper = new MyRetweetsHelper(retweets.getItems());
                        twitterHelper.load();
                    }
                } catch (TwitterException te) {
                    LOG.error("An error occured while retrieving your mentions", te);
                    error_display.setText(resources.getString("error_retrieving_mentions"));
                }
            }
        };
        thread.start();
    }

    private void loadMoreRetweets() {
        if (twitterHelper != null) {
            try {
                if (selected.compareTo(YOUR_TWEETS_RETWEETED) == 0) {
                    twitterHelper = new MyTweetsRetweetedHelper(retweets.getItems());
                    twitterHelper.load();
                } else {
                    twitterHelper = new MyRetweetsHelper(retweets.getItems());
                    twitterHelper.load();
                }
            } catch (TwitterException te) {
                LOG.error("An error occured while retrieving your mentions", te);
                error_display.setText(resources.getString("error_retrieving_mentions"));
            }
        }
    }
}
