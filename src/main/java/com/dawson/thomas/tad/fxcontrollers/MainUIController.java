package com.dawson.thomas.tad.fxcontrollers;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Thomas
 */
public class MainUIController {
    
    private final static Logger LOG = LoggerFactory.getLogger(MainUIController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    @FXML
    private BorderPane master_pane;
    @FXML
    private Label title_bar;
    @FXML
    private Button timeline_button;
    @FXML
    private Button send_tweet_button;
    @FXML
    private Button retweets_button;
    @FXML
    private Button mentions_button;
    @FXML
    private Button saved_tweets_button;
    @FXML
    private Button messages_button;
    @FXML
    private Button search_button;
    @FXML
    private Button help_button;
    
    private Node timelineModule;
    private Node sendTweetModule;
    private Node retweetsModule;
    private Node mentionsModule;
    private Node savedTweetsModule;
    private GridPane messagesModule;
    private Node searchModule;
    private Node helpModule;
    private GridPane sendMessageModule;
    
    private MessagesModuleController messagesController;
    private SendMessageModuleController sendMessageController;
    
    public MainUIController() {
        super();
    }
    
    @FXML
    private void initialize() {
        LOG.debug("initializing main ui");
        createTimelineModule();
        createSendTweetModule();
        createRetweetsModule();
        createMentionsModule();
        createSavedTweetsModule();
        createMessagesModule();
        createSearchModule();
        createHelpModule();
        
        messagesController.passUIHandles(master_pane, sendMessageModule);
        
        timeline_button.setOnAction(e -> {master_pane.setCenter(timelineModule); updateTitleBar("Home_timeline");});
        send_tweet_button.setOnAction(e -> {master_pane.setCenter(sendTweetModule); updateTitleBar("Send_tweets");});
        retweets_button.setOnAction(e -> {master_pane.setCenter(retweetsModule); updateTitleBar("Retweets");});
        mentions_button.setOnAction(e -> {master_pane.setCenter(mentionsModule); updateTitleBar("Mentions");});
        saved_tweets_button.setOnAction(e -> {master_pane.setCenter(savedTweetsModule); updateTitleBar("Saved_tweets");});
        messages_button.setOnAction(e -> {master_pane.setCenter(messagesModule); updateTitleBar("Direct_messages");});
        search_button.setOnAction(e -> {master_pane.setCenter(searchModule); updateTitleBar("Search_twitter");});
        help_button.setOnAction(e -> {master_pane.setCenter(helpModule); updateTitleBar("Help");});
    }
    
    private void createTimelineModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/TimelineModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            timelineModule = loader.load();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    private void createSendTweetModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/SendTweetModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            sendTweetModule = loader.load();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    private void createRetweetsModule() {
        LOG.info("creating retweets module");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/RetweetsModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            retweetsModule = loader.load();
            LOG.info("created retweets module");
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    private void createMentionsModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MentionsModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            mentionsModule = loader.load();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    private void createSavedTweetsModule(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/SavedTweetsModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            savedTweetsModule = loader.load();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    private void createMessagesModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessagesModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            messagesModule = loader.load();
            messagesController = loader.getController();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
        createSendMessageModule();
        messagesController.setSendMessageController(sendMessageController);
    }
    
    private void createSearchModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/SearchModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            searchModule = loader.load();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    private void createHelpModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/HelpModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            helpModule = loader.load();
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }

    private void createSendMessageModule() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/SendMessageModule.fxml"));
            loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
            sendMessageModule = loader.load();
            sendMessageController = loader.getController();
            sendMessageController.passUIHandles(master_pane, messagesModule);
        } catch (IOException ioe) {
            LOG.error("Unable to open module", ioe);
        }
    }
    
    public void openTimeline() {
        master_pane.setCenter(timelineModule);
        updateTitleBar("Home_timeline");
    }
    
    private void updateTitleBar(String key){
        title_bar.setText(resources.getString("Title_bar_text") + " - " + resources.getString(key));
    }
}
