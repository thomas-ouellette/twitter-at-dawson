/**
 * Package containing all UI controllers. Presentation layer classes 
 * which talk to/interact with business layer objects.
 * 
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.TwitterEngine;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Paint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class SendTweetModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(SendTweetModuleController.class);

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML 
    private Label char_count; 
    @FXML 
    private Button send_tweet;
    @FXML 
    private TextArea tweet_text;
    @FXML 
    private Label message_display; 

    @FXML 
    private void initialize() {
        tweet_text.setOnKeyTyped(e -> limitTweetLength());
        send_tweet.setOnAction(e -> sendTweet());
    }

    private void limitTweetLength() {
        resetDisplay();
        send_tweet.setDisable(false);
        String text = tweet_text.getText();
        char_count.setText(text.length() + "/280");
        if (text.length() > 280) {
            tweet_text.deletePreviousChar();
            char_count.setText(text.length() - 1 + "/280");
        }
    }

    private void sendTweet() {
        String tweet = tweet_text.getText().trim();
        if (!tweet.isEmpty()) {
            if (tweet.length() <= 280) {
                resetDisplay();
                TwitterEngine twitter = new TwitterEngine();
                try {
                    twitter.createTweet(tweet);
                    displaySuccess("success_send_tweet");
                    send_tweet.setDisable(true);
                    tweet_text.setText("");
                } catch (TwitterException te) {
                    LOG.error(resources.getString("error_tweet_not_sent"), te);
                    displayError("error_tweet_not_sent");
                }
            } else {
                displayError("error_too_long");
            }
        }
    }
    
    private void resetDisplay(){
        message_display.setText("");
    } 
    
    private void displayError(String key){
        message_display.setTextFill(Paint.valueOf("RED"));
        message_display.setText(resources.getString(key));
    }
    
    private void displaySuccess(String key){
        message_display.setTextFill(Paint.valueOf("GREEN"));
        message_display.setText(resources.getString(key));
    }
}
