/**
 * Package containing all UI controllers. Presentation layer classes 
 * which talk to/interact with business layer objects.
 * 
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.TimelineHelper;
import com.dawson.thomas.tad.presentation.TweetCell;
import com.dawson.thomas.tad.data.TwitterInfo;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import com.dawson.thomas.tad.business.ITwitterPagingHelper;

/**
 * FXML Controller class
 *
 * @author Thomas Ouellette
 */
public class TimelineModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(TimelineModuleController.class);

    @FXML
    private ResourceBundle resources;
    @FXML
    private ListView<TwitterInfo> timeline;
    @FXML
    private Text error_display;
    @FXML
    private Button load_more_button;
    @FXML
    private Button refresh_button;

    private ITwitterPagingHelper twitterHelper;

    public TimelineModuleController() {
        super();
    }

    @FXML
    private void initialize() {
        setupListView();
        refreshTimeline();

        refresh_button.setOnAction(e -> refreshTimeline());
        load_more_button.setOnAction(e -> loadMoreTweets());
    }

    private void setupListView() {
        ObservableList<TwitterInfo> list = FXCollections.observableArrayList();
        timeline.setItems(list);
        timeline.setCellFactory(p -> new TweetCell());
    }

    private void refreshTimeline() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    timeline.getItems().clear();
                    twitterHelper = new TimelineHelper(timeline.getItems());
                    twitterHelper.load();
                } catch (TwitterException te) {
                    LOG.error("An error occured while retrieving the timeline", te);
                    error_display.setText(resources.getString("error_retrieving_timeline"));
                }
            }
        };
        thread.start();
    }

    private void loadMoreTweets() {
        if (twitterHelper != null) {
            try {
                twitterHelper.load();
            } catch (TwitterException te) {
                LOG.error("An error occured while retrieving the timeline", te);
                error_display.setText(resources.getString("error_retrieving_timeline"));
            }
        }
    }
}
