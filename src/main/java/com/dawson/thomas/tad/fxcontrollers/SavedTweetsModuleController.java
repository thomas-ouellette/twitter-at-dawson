/**
 * Package containing all UI controllers. Presentation layer classes 
 * which talk to/interact with business layer objects.
 * 
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.presentation.SavedTweetCell;
import com.dawson.thomas.tad.business.SavedTweetsHelper;
import com.dawson.thomas.tad.data.TwitterInfoDB;
import com.dawson.thomas.tad.persistence.TweetDAO;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SavedTweetsModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(SavedTweetsModuleController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="timeline"
    private ListView<TwitterInfoDB> saved_tweets; // Value injected by FXMLLoader

    @FXML // fx:id="error_display"
    private Text error_display; // Value injected by FXMLLoader

    @FXML // fx:id="refresh_button"
    private Button refresh_button; // Value injected by FXMLLoader

    @FXML // fx:id="load_more_button"
    private Button load_more_button; // Value injected by FXMLLoader
    
    private SavedTweetsHelper pagingHelper;
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        setupListView();
        refreshList();
        refresh_button.setOnAction(e -> refreshList());
        load_more_button.setOnAction(e -> loadMore());
    }
    
    private void setupListView() {
        ObservableList<TwitterInfoDB> list = FXCollections.observableArrayList();
        saved_tweets.setItems(list);
        saved_tweets.setCellFactory(p -> new SavedTweetCell(this));
    }
    
    private void refreshList(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    saved_tweets.getItems().clear();
                    pagingHelper = new SavedTweetsHelper(saved_tweets.getItems());
                    pagingHelper.load();
                } catch (SQLException | IOException e) {
                    LOG.error("An error occured while retrieving saved tweets", e);
                    error_display.setText(resources.getString("error_retrieving_saved_tweets"));
                }
            }
        };
        thread.start();
    }
    
    private void loadMore(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    pagingHelper.load();
                } catch (SQLException | IOException e) {
                    LOG.error("An error occured while retrieving saved tweets", e);
                    error_display.setText(resources.getString("error_retrieving_saved_tweets"));
                }
            }
        };
        thread.start();
    }
    
    protected void unsaveTweet(long id){
        try {
            TweetDAO tweetDAO = new TweetDAO();
            tweetDAO.unsaveTweet(id);
            saved_tweets.getItems().removeIf(tweet -> tweet.getStatusId() == id);
        } catch (SQLException | IOException ex) {
            java.util.logging.Logger.getLogger(SavedTweetsModuleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
