/**
 * Package containing all UI controllers. Presentation layer classes 
 * which talk to/interact with business layer objects.
 * 
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.presentation.MessageCell;
import com.dawson.thomas.tad.business.TwitterEngine;
import com.dawson.thomas.tad.data.MessageBean;
import com.dawson.thomas.tad.data.UserBean;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class SendMessageModuleController {
    
    private final static Logger LOG = LoggerFactory.getLogger(SendMessageModuleController.class);

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML 
    private Button send_message;
    @FXML 
    private Button back_button;
    @FXML
    private TextField message; 
    @FXML
    private ListView messages;
    @FXML 
    private Label error_display;
    @FXML
    private Label username;

    private BorderPane mainUI;

    private GridPane messagesModule;
    
    private TwitterEngine twitter;
    
    private UserBean user;
    
    private long myId = -1L;
    
    @FXML 
    void initialize() {
        setupListView();
        twitter = new TwitterEngine();
        try {
            myId = twitter.getTwitterInstance().getId();
        } catch (TwitterException | IllegalStateException ex) {
            LOG.error("Unable to get user id", ex);
        }
        send_message.setOnAction(e -> sendMessage());
        back_button.setOnAction(e -> mainUI.setCenter(messagesModule));
    }

    private void sendMessage() {
        String msg = message.getText().trim();
        if(!msg.isBlank()){
            message.setText("");
            try {
                twitter.sendMessage(msg, user.getUserId());
                messages.getItems().add(new MessageBean(myId, msg));
            } catch (TwitterException ex) {
                LOG.error("Unable to send message", ex);
            }
        }
    }

    public void passUIHandles(BorderPane mainUI, GridPane messagesModule) {
        this.mainUI = mainUI;
        this.messagesModule = messagesModule;
    }
    
    public void setUser(UserBean user){
        this.user = user;
        username.setText(user.getUsername());
        refreshListView();
    }
    
    private void setupListView(){
        messages.setItems(FXCollections.observableArrayList());
        messages.setCellFactory(p -> new MessageCell());
    }
    
    private void refreshListView() {
        messages.getItems().clear();
        if (twitter != null && user != null) {
            try {
                twitter.getConversation(user.getUserId()).stream().forEach(msg -> messages.getItems().add(msg));
            } catch (TwitterException ex) {
                LOG.error("Coudln't retrieve messages", ex);
            }
        }
    }
}

