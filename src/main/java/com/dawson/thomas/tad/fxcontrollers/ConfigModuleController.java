package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.data.DatabaseProperties;
import com.dawson.thomas.tad.data.TwitterAPIProperties;
import com.dawson.thomas.tad.manager.DatabasePropertiesManager;
import com.dawson.thomas.tad.manager.TwitterAPIPropertiesManager;
import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Thomas
 */
public class ConfigModuleController {
    private final static Logger LOG = LoggerFactory.getLogger(ConfigModuleController.class);
    
    @FXML
    private TextField consumerKey;
    
    @FXML
    private TextField consumerSecret;
    
    @FXML
    private TextField accessToken;
    
    @FXML
    private TextField accessTokenSecret;
    
    @FXML // fx:id="dbName"
    private TextField dbName; // Value injected by FXMLLoader

    @FXML // fx:id="dbUsername"
    private TextField dbUsername; // Value injected by FXMLLoader

    @FXML // fx:id="dbPassword"
    private TextField dbPassword; // Value injected by FXMLLoader

    @FXML // fx:id="dbURL"
    private TextField dbURL; // Value injected by FXMLLoader

    @FXML // fx:id="dbPort"
    private TextField dbPort; // Value injected by FXMLLoader
    
    @FXML
    private Button submit;
    
    private TwitterAPIProperties apiKeys;
    private DatabaseProperties dbProps;
    
    private DatabasePropertiesManager dbPropsManager;
    private TwitterAPIPropertiesManager twitterPropsManager;
    
    private Stage stage;
    private Scene mainUI;
    
    public ConfigModuleController(){
        super();
        apiKeys = new TwitterAPIProperties();
        dbProps = new DatabaseProperties();
    }
    
    @FXML
    private void initialize(){
        
    }
    
    public void setUIComponents(Scene mainUI, Stage stage) {
        this.mainUI = mainUI;
        this.stage = stage;
    }
    
    public void setPropertiesManagers(TwitterAPIPropertiesManager twitterPropsManager, DatabasePropertiesManager dbPropsManager){
        this.dbPropsManager = dbPropsManager;
        this.twitterPropsManager = twitterPropsManager;
    }
    
    public void setProperties(TwitterAPIProperties apiKeys, DatabaseProperties dbProps){
        this.apiKeys = apiKeys;
        this.dbProps = dbProps;
        
        bindProperties();
    }
    
    @FXML
    void saveProps(ActionEvent event){
        try{
            twitterPropsManager.writeTextProperties("./src/main/resources/", "twitter4j", apiKeys);
            dbPropsManager.writeTextProperties("./src/main/resources/", "database_properties", dbProps);
            stage.setScene(mainUI);
        } catch(IOException ioe){
            LOG.error("Couldn't write properties to file", ioe);
        }
    }
    
    private void bindProperties(){
        // Twitter API Properties
        Bindings.bindBidirectional(consumerKey.textProperty(), apiKeys.consumerKeyProperty());
        Bindings.bindBidirectional(consumerSecret.textProperty(), apiKeys.consumerSecretProperty());
        Bindings.bindBidirectional(accessToken.textProperty(), apiKeys.accessTokenProperty());
        Bindings.bindBidirectional(accessTokenSecret.textProperty(), apiKeys.accessTokenSecretProperty());
        
        // Database properties
        Bindings.bindBidirectional(dbName.textProperty(), dbProps.dbName());
        Bindings.bindBidirectional(dbUsername.textProperty(), dbProps.dbUsername());
        Bindings.bindBidirectional(dbPassword.textProperty(), dbProps.dbPassword());
        Bindings.bindBidirectional(dbURL.textProperty(), dbProps.dbURL());
        Bindings.bindBidirectional(dbPort.textProperty(), dbProps.dbPort());
    }
}
