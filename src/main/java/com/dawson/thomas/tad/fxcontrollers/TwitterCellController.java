/**
 * Package containing all UI controllers. Presentation layer classes
 * which talk to/interact with business layer objects.
 *
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.TwitterEngine;
import com.dawson.thomas.tad.data.TwitterInfo;
import com.dawson.thomas.tad.persistence.TweetDAO;
import java.io.IOException;
import java.net.URL;
import static java.nio.file.Paths.get;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Controller used for the behaviour of all tweet cells in all modules that
 * display lists of tweets except for the saved tweets module.
 *
 * @author Thomas
 */
public class TwitterCellController {

    private final static Logger LOG = LoggerFactory.getLogger(TwitterCellController.class);

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private HBox tweet_cell;
    @FXML
    private ImageView profile_pic;
    @FXML
    private Text name;
    @FXML
    private Text twitter_handle;
    @FXML
    private Text date;
    @FXML
    private Text tweet_text;
    @FXML
    private Button button_reply;
    @FXML
    private Text num_replies;
    @FXML
    private Button button_retweet;
    @FXML
    private Text num_retweets;
    @FXML
    private ImageView image_retweet;
    @FXML
    private Button button_like;
    @FXML
    private ImageView image_like;
    @FXML
    private Text num_likes;
    @FXML
    private Button button_save;
    @FXML
    private ImageView image_save;

    private TwitterInfo twitterInfo;

    private TwitterEngine twitter;

    private TweetDAO tweetDAO;

    private boolean isSaved = false;

    @FXML
    void initialize() {
        tweetDAO = new TweetDAO();
        button_save.setOnAction(e -> changeSavedStatus());
        button_like.setOnAction(e -> changeLikedStatus());
        button_retweet.setOnAction(e -> changeRetweetStatus());
    }

    public void setTwitterInfo(TwitterInfo twitterInfo) {
        this.twitterInfo = twitterInfo;
        populateFields();
        checkIfIsSaved();
        checkIfIsLiked();
        checkIfIsRetweeted();
    }

    private void populateFields() {
        if (twitterInfo != null) {
            twitter_handle.setText("@" + twitterInfo.getHandle());
            date.setText(twitterInfo.getDate().toString().substring(0, 16));
            tweet_text.setText(twitterInfo.getText());
            name.setText(twitterInfo.getName());
            Image image = new Image(twitterInfo.getImageURL(), 48, 48, true, false);
            profile_pic.setImage(image);
            updateLikes();
            int retweets = twitterInfo.getRetweets();
            num_retweets.setText(retweets == 0 ? "" : retweets + "");
            int replies = 0;
            try {
                replies = twitterInfo.getReplies();
            } catch (TwitterException te) {
                LOG.error("Couldn't get comments", te);
            }
            num_replies.setText(replies == 0 ? "" : replies + "");
        }
    }

    private void checkIfIsSaved() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (twitterInfo != null) {
                    try {
                        isSaved = tweetDAO.isSaved(twitterInfo.getId());
                        if (isSaved) {
                            showSaved();
                        }
                    } catch (SQLException | IOException ex) {
                        LOG.error("Unable to check if tweet is saved", ex);
                    }

                }
            }
        };
        thread.start();
    }

    private void checkIfIsLiked() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (twitterInfo != null) {
                    if (twitterInfo.isFavorited()) {
                        showLiked();
                    }

                }
            }
        };
        thread.start();
    }

    private void checkIfIsRetweeted() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (twitterInfo != null) {
                    if (twitterInfo.isRetweetedByMe()) {
                        showRetweeted();
                    }

                }
            }
        };
        thread.start();
    }

    private void changeSavedStatus() {
        if (twitterInfo != null) {
            try {
                if (isSaved) {
                    tweetDAO.unsaveTweet(twitterInfo.getId());
                    showUnsaved();
                } else {
                    tweetDAO.saveTweet(twitterInfo.getTwitterInfoDB());
                    showSaved();
                }
            } catch (SQLException | IOException ex) {
                LOG.error("Unable to (un)save tweet", ex);
            }
        }
    }

    private void changeLikedStatus() {
        LOG.info("Like pressed");
        if (twitterInfo != null && twitter != null) {
            if (twitterInfo.isFavorited()) {
                try {
                    twitter.destroyFavorite(twitterInfo.getId());
                } catch (TwitterException te) {
                    LOG.error("Couldn't unfavorite tweet", te);
                }
                showNotLiked();
            } else {
                try {
                    twitter.createFavorite(twitterInfo.getId());
                } catch (TwitterException te) {
                    LOG.error("Couldn't favorite tweet", te);
                }

                showLiked();
            }

            updateLikes();
        }
    }

    private void changeRetweetStatus() {
        if (twitterInfo != null && twitter != null) {
            if (twitterInfo.isRetweetedByMe()) {
                try {
                    twitter.removeRetweet(twitterInfo.getId());
                } catch (TwitterException te) {
                    LOG.error("Couldn't favorite tweet", te);
                }
                showNotRetweeted();
            } else {
                try {
                    twitter.createRetweet(twitterInfo.getId());
                } catch (TwitterException te) {
                    LOG.error("Couldn't favorite tweet", te);
                }
                showRetweeted();
            }
        }
    }

    private void showSaved() {
        Image image = new Image(get("./src/main/resources/images", "saved.png").toUri().toString());
        image_save.setImage(image);
    }

    private void showUnsaved() {
        Image image = new Image(get("./src/main/resources/images", "save.png").toUri().toString());
        image_save.setImage(image);
    }

    private void showLiked() {
        Image image = new Image(get("./src/main/resources/images", "liked.png").toUri().toString());
        image_like.setImage(image);

    }

    private void showNotLiked() {
        Image image = new Image(get("./src/main/resources/images", "like.png").toUri().toString());
        image_like.setImage(image);
    }

    private void updateLikes() {
        if (twitterInfo != null) {
            int likes = twitterInfo.getLikes();
            num_likes.setText(likes == 0 ? "" : likes + "");
        }
    }

    private void showRetweeted() {
        Image image = new Image(get("./src/main/resources/images", "retweeted.png").toUri().toString());
        image_retweet.setImage(image);
    }

    private void showNotRetweeted() {
        Image image = new Image(get("./src/main/resources/images", "retweet.png").toUri().toString());
        image_retweet.setImage(image);
    }
}
