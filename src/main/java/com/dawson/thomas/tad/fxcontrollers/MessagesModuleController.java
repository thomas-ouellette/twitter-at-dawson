/**
 * Sample Skeleton for 'MessagesModule.fxml' Controller Class
 */
package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.TwitterEngine;
import com.dawson.thomas.tad.presentation.UserCell;
import com.dawson.thomas.tad.data.UserBean;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class MessagesModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(MessagesModuleController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="messages_list"
    private ListView<UserBean> users_list; // Value injected by FXMLLoader

    @FXML
    private GridPane messages_module;
    
    private BorderPane mainUI;
    
    private GridPane sendMessageModule;
    
    private SendMessageModuleController sendMessageController;
    
    TwitterEngine twitter;
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        users_list.setItems(FXCollections.observableArrayList());
        users_list.setCellFactory(p -> new UserCell());
        twitter = new TwitterEngine();
        try {
            refresh();
        } catch (TwitterException te) {
            LOG.error("Couldn't get messages", te);
        }
        users_list.getSelectionModel().selectedItemProperty().addListener(cl -> {
            sendMessageController.setUser(users_list.getSelectionModel().getSelectedItem());
            mainUI.setCenter(sendMessageModule); 
        });
    }
    
    public void passUIHandles(BorderPane mainUI, GridPane sendMessageModule){
        this.mainUI = mainUI;
        this.sendMessageModule = sendMessageModule;
    }
    
    public void setSendMessageController(SendMessageModuleController sendMessageController){
        this.sendMessageController = sendMessageController;
    }
    
    public void refresh() throws TwitterException{
        twitter.getUsersWhoHaveMessagedMe().stream().forEach(user -> users_list.getItems().add(user));
    }
}
