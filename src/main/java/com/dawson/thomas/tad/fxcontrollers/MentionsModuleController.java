/**
 * Sample Skeleton for 'MentionsModule.fxml' Controller Class
 */

package com.dawson.thomas.tad.fxcontrollers;

import com.dawson.thomas.tad.business.MentionsHelper;
import com.dawson.thomas.tad.presentation.TweetCell;
import com.dawson.thomas.tad.data.TwitterInfo;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;
import com.dawson.thomas.tad.business.ITwitterPagingHelper;

public class MentionsModuleController {

    private final static Logger LOG = LoggerFactory.getLogger(MentionsModuleController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="mentions"
    private ListView<TwitterInfo> mentions; // Value injected by FXMLLoader

    @FXML // fx:id="refresh_button"
    private Button refresh_button; // Value injected by FXMLLoader

    @FXML // fx:id="load_more_button"
    private Button load_more_button; // Value injected by FXMLLoader
    
    @FXML
    private Text error_display;
    
    private ITwitterPagingHelper twitterHelper;

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        setupListView();
        refreshTimeline();

        refresh_button.setOnAction(e -> refreshTimeline());
        load_more_button.setOnAction(e -> loadMoreTweets());
    }

    private void setupListView() {
        ObservableList<TwitterInfo> list = FXCollections.observableArrayList();
        mentions.setItems(list);
        mentions.setCellFactory(p -> new TweetCell());
    }

    private void refreshTimeline() {
        Thread thread = new Thread() {
            public void run() {
                try {
                    mentions.getItems().clear();
                    twitterHelper = new MentionsHelper(mentions.getItems());
                    twitterHelper.load();
                } catch (TwitterException te) {
                    LOG.error("An error occured while retrieving your mentions", te);
                    error_display.setText(resources.getString("error_retrieving_mentions"));
                }
            }
        };
        thread.start();
    }

    private void loadMoreTweets() {
        if (twitterHelper != null) {
            try {
                twitterHelper.load();
            } catch (TwitterException te) {
                LOG.error("An error occured while retrieving your mentions", te);
                error_display.setText(resources.getString("error_retrieving_mentions"));
            }
        }
    }
}
