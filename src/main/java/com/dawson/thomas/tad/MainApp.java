/**
 * This is the root package of the application.
 *
 * <p>
 * Its only class is MainApp - the starting point of the application.
 */
package com.dawson.thomas.tad;

import com.dawson.thomas.tad.data.DatabaseProperties;
import com.dawson.thomas.tad.data.TwitterAPIProperties;
import com.dawson.thomas.tad.fxcontrollers.ConfigModuleController;
import com.dawson.thomas.tad.fxcontrollers.MainUIController;
import com.dawson.thomas.tad.manager.DatabasePropertiesManager;
import com.dawson.thomas.tad.manager.TwitterAPIPropertiesManager;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the starting point of this application.
 *
 * @author Thomas
 */
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    /**
     * Constants used for the minimum width and height of the application stage.
     */
    private final static double MIN_WIDTH = 616.0;
    private final static double MIN_HEIGHT = 482.0;

    private Stage stage;

    /**
     * Application user interfaces are created by helper methods and assigned to
     * these variables.
     *
     * <p>
     * If the application configuration file exists and contains non-empty
     * values, the main user interface is loaded onto the stage. Otherwise, the
     * configuration module opens.
     */
    private Scene mainUI;
    private Scene configModule;

    private TwitterAPIProperties apiKeys;
    private DatabasePropertiesManager dbPropsManager;
    private TwitterAPIPropertiesManager twitterPropsManager;
    private DatabaseProperties dbProps;

    public MainApp() {
        super();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        this.stage = stage;

        twitterPropsManager = new TwitterAPIPropertiesManager();
        dbPropsManager = new DatabasePropertiesManager();
        apiKeys = new TwitterAPIProperties();
        dbProps = new DatabaseProperties();

        try {
            createMainUI();
            // Checks for existence of properties
            if (propertiesAreThere()) {
                // Jump into the main application
                stage.setScene(mainUI);
            } else {
                // Loads configuration module
                createConfigModule();
                stage.setScene(configModule);
            }
        } catch (IOException e) {
            LOG.error("Couldn't load properties file", e);
            stage.setScene(configModule);
        }

        stage.setMinHeight(MIN_HEIGHT);
        stage.setMinWidth(MIN_WIDTH);
        stage.setTitle("Twitter @ Dawson");
        stage.show();
    }

    /**
     * Method responsible for inflating configuration module user interface.
     *
     * @throws IOException
     */
    private void createConfigModule() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ConfigModule.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
        configModule = new Scene(loader.load());
        ConfigModuleController controller = loader.getController();
        controller.setUIComponents(mainUI, stage);
        controller.setPropertiesManagers(twitterPropsManager, dbPropsManager);
        controller.setProperties(apiKeys, dbProps);
    }

    /**
     * Method responsible for inflating main user interface.
     *
     * @throws IOException
     */
    private void createMainUI() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/MainUI.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));
        mainUI = new Scene(loader.load());
        MainUIController controller = loader.getController();
        controller.openTimeline();
    }

    /**
     * Helper method that checks if a TwitterAPIProperties bean's properties are
     * all not empty.
     *
     * @param props
     * @return {@code true} if none of the properties are empty, {@code false}
     * otherwise.
     */
    private boolean twitterPropsNotEmpty(TwitterAPIProperties props) {
        return !props.getAccessToken().isEmpty() 
                && !props.getAccessTokenSecret().isEmpty()
                && !props.getConsumerKey().isEmpty() 
                && !props.getConsumerSecret().isEmpty();
    }

    /**
     * Helper method that checks if a DatabaseProperties bean's properties are
     * all not empty.
     *
     * @param props
     * @return {@code true} if none of the properties are empty, {@code false}
     * otherwise.
     */
    private boolean databasePropsNotEmpty(DatabaseProperties props) {
        return !props.getDbName().isEmpty() 
                && !props.getDbUsername().isEmpty()
                && !props.getDbPassword().isEmpty() 
                && !props.getDbURL().isEmpty() 
                && !props.getDbPort().isEmpty();
    }

    // check if files are there and if the properties are not empty
    private boolean propertiesAreThere() throws IOException {
        boolean hasTwitterProps = false;
        boolean hasDbProps = false;

        if (twitterPropsManager.loadTextProperties(apiKeys, "./src/main/resources/", "twitter4j") && twitterPropsNotEmpty(apiKeys)) {
            hasTwitterProps = true;
        }

        if (dbPropsManager.loadTextProperties(dbProps, "./src/main/resources/", "database_properties") && databasePropsNotEmpty(dbProps)) {
            hasDbProps = true;
        }

        return hasTwitterProps && hasDbProps;
    }
}
