/**
 * The data package consists of Java data beans that are used to holding 
 * information which can be read from every bean and written to some.
 * 
 */
package com.dawson.thomas.tad.data;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * This data bean is used to hold a Status object which represents a 
 * tweet and has methods used for accessing it's fields.
 * 
 * @see twitter4j.Status
 * @author Thomas
 */
public class TwitterInfo {
    
    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfo.class);
    
    private final Status status;
    
    private final Twitter twitter;
    
    public TwitterInfo(Status status){
        this.status = status;
        twitter = TwitterFactory.getSingleton();
    }
    
    public long getId(){
        return status.getId();
    }
    
    public String getName() {
        return status.getUser().getName();
    }

    public String getText(){
        return status.getText();
    }

    public String getImageURL(){
        return status.getUser().getProfileImageURL();
    }
    
    public String getHandle() {
      return status.getUser().getScreenName();
    }
    
    public int getLikes(){
      return status.getFavoriteCount();
    }
    
    public int getRetweets(){
      return status.getRetweetCount();
    }
    
    public int getReplies() throws TwitterException {
        Query query = new Query("to:"+getHandle());
        
        query.setSinceId(status.getId());
        
        // This will retrieve all the tweets that are comments
        QueryResult result = twitter.search(query);
        
        // This turns the QueryResult into a list
        List<Status> statuses = result.getTweets();
        
        // Return the size of the list
        return statuses.size();
    }
    
    public Date getDate(){
        return status.getCreatedAt();
    }
    
    public TwitterInfoDB getTwitterInfoDB(){
        return new TwitterInfoDB(this.getId(), "saved", this.getName(), this.getHandle(), this.getText(), this.getDate());
    }
    
    public boolean isFavorited(){
        return status.isFavorited();
    }
    
    public boolean isRetweetedByMe(){
        return status.isRetweetedByMe();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.status);
        hash = 79 * hash + Objects.hashCode(this.twitter);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwitterInfo other = (TwitterInfo) obj;
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        return Objects.equals(this.twitter, other.twitter);
    }
    
}
