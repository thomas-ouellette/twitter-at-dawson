/**
 * The data package consists of Java data beans that are used to holding 
 * information which can be read from every bean and written to some.
 * 
 */
package com.dawson.thomas.tad.data;

/**
 * Data bean which represents a user who has sent the authenticated 
 * user a direct message.
 * 
 * @author Thomas
 */
public class UserBean {
    private final long userId;
    private final String username;
    private final String lastMessage;
    
    public UserBean(){
        this(-1L, "", "");
    }
    
    public UserBean(long userId, String username, String lastMessage){
        this.userId = userId;
        this.username = username;
        this.lastMessage = lastMessage;
    }
    
    public long getUserId(){
        return this.userId;
    }
    
    public String getUsername(){
        return this.username;
    }
    
    public String getLastMessage(){
        return this.lastMessage;
    }
}
