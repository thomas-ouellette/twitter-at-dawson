/**
 * The data package consists of Java data beans that are used to holding 
 * information which can be read from every bean and written to some.
 * 
 */
package com.dawson.thomas.tad.data;

/**
 * This data bean is used for holding the data needed for each message 
 * displayed to the user: the sender and the text content.
 * 
 * @author Thomas
 */
public class MessageBean {
    private final long sender;
    private final String text;
    
    public MessageBean(){
        this(-1L, "");
    }
    
    public MessageBean(long sender, String text){
        this.sender = sender;
        this.text = text;
    }
    
    public long getSender(){
        return this.sender;
    }
    
    public String getText(){
        return this.text;
    }
}
