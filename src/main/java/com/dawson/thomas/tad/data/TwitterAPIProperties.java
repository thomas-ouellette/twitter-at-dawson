/**
 * The data package consists of Java data beans that are used to holding 
 * information which can be read from every bean and written to some.
 * 
 */
package com.dawson.thomas.tad.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Java bean used for holding the Twitter API properties 
 * (i.e. the 4 developer keys needed for the Twitter API)
 * 
 * <p>
 * These keys are stored in a properties file for use by the Twitter4J library.
 * 
 * @see TwitterAPIPropertiesManager
 * @author Thomas
 */
public class TwitterAPIProperties {
    private final StringProperty consumerKey;
    private final StringProperty consumerSecret;
    private final StringProperty accessToken;
    private final StringProperty accessTokenSecret;
    
    public TwitterAPIProperties(){
        this("", "", "", "");
    }
    
    public TwitterAPIProperties(final String consumerKey, final String consumerSecret, final String accessToken, final String accessTokenSecret){
        super();
        this.consumerKey = new SimpleStringProperty(consumerKey);
        this.consumerSecret = new SimpleStringProperty(consumerSecret);
        this.accessToken = new SimpleStringProperty(accessToken);
        this.accessTokenSecret = new SimpleStringProperty(accessTokenSecret);
    }
    
    public String getConsumerKey(){
        return this.consumerKey.get();
    }
    
    public String getConsumerSecret(){
        return this.consumerSecret.get();
    }
    
    public String getAccessToken(){
        return this.accessToken.get();
    }
    
    public String getAccessTokenSecret(){
        return this.accessTokenSecret.get();
    }
    
    public void setConsumerKey(String consumerKey){
        this.consumerKey.set(consumerKey);
    }
    
    public void setConsumerSecret(String consumerSecret){
        this.consumerSecret.set(consumerSecret);
    }
    
    public void setAccessToken(String accessToken){
        this.accessToken.set(accessToken);
    }
    
    public void setAccessTokenSecret(String accessTokenSecret){
        this.accessTokenSecret.set(accessTokenSecret);
    }
    
    public StringProperty consumerKeyProperty(){
        return this.consumerKey;
    }
    
    public StringProperty consumerSecretProperty(){
        return this.consumerSecret;
    }
    
    public StringProperty accessTokenProperty(){
        return this.accessToken;
    }
    
    public StringProperty accessTokenSecretProperty(){
        return this.accessTokenSecret;
    }
    
    // TO-DO equals + hashCode, toString
}
