/**
 * The data package consists of Java data beans that are used to holding 
 * information which can be read from every bean and written to some.
 * 
 */
package com.dawson.thomas.tad.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This data bean is used for holding the configuration information necessary 
 * to use the local MySQL database.
 * 
 * @author Thomas
 */
public class DatabaseProperties {
    private final StringProperty dbName;
    private final StringProperty dbUsername;
    private final StringProperty dbPassword;
    private final StringProperty dbURL;
    private final StringProperty dbPort;
    
    public DatabaseProperties(){
        this("", "", "", "", "");
    }
    
    public DatabaseProperties(final String dbName, final String dbUsername, final String dbPassword, final String dbURL, final String dbPort){
        super();    
        this.dbName = new SimpleStringProperty(dbName);
        this.dbUsername = new SimpleStringProperty(dbUsername);
        this.dbPassword = new SimpleStringProperty(dbPassword);
        this.dbURL = new SimpleStringProperty(dbURL);
        this.dbPort = new SimpleStringProperty(dbPort);
    }

    // Getters
    public String getDbName() {
        return dbName.get();
    }

    public String getDbUsername() {
        return dbUsername.get();
    }

    public String getDbPassword() {
        return dbPassword.get();
    }

    public String getDbURL() {
        return dbURL.get();
    }

    public String getDbPort() {
        return dbPort.get();
    }
    
    // Setters
    public void setDbName(String dbName) {
        this.dbName.set(dbName);
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername.set(dbUsername);
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword.set(dbPassword);
    }

    public void setDbURL(String dbURL) {
        this.dbURL.set(dbURL);
    }

    public void setDbPort(String dbPort) {
        this.dbPort.set(dbPort);
    }
    
    // Properties
    public StringProperty dbName() {
        return dbName;
    }

    public StringProperty dbUsername() {
        return dbUsername;
    }

    public StringProperty dbPassword() {
        return dbPassword;
    }

    public StringProperty dbURL() {
        return dbURL;
    }

    public StringProperty dbPort() {
        return dbPort;
    }
}
