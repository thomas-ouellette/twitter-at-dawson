/**
 * The data package consists of Java data beans that are used to holding 
 * information which can be read from every bean and written to some.
 * 
 */
package com.dawson.thomas.tad.data;

import java.util.Date;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bean used for storing and retrieving tweets from local database. 
 * It is similar to TwitterInfo except that it does not hold a Status
 * object and does contain setter/mutaters.
 * 
 * @author Thomas
 */
public class TwitterInfoDB {
    
    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfoDB.class);
    
    private long status_id;
    private String tweet_category;
    private String screen_name;
    private String username;
    private String tweet_text;
    private Date created_at_date;
    
    public TwitterInfoDB(){
        this(0L, "", "", "", "", null);
    }
    
    
    public TwitterInfoDB(long status_id, String tweet_category, String screen_name, String username, String tweet_text, Date created_at_date){
        super();
        this.status_id = status_id;
        this.tweet_category = tweet_category;
        this.screen_name = screen_name;
        this.username = username;
        this.tweet_text = tweet_text;
        this.created_at_date = created_at_date;
    }
    
    public TwitterInfoDB(TwitterInfo twitterInfo){
        super();
        this.status_id = twitterInfo.getId();
        this.tweet_category = "";
        this.screen_name = twitterInfo.getName();
        this.username = twitterInfo.getHandle();
        this.tweet_text = twitterInfo.getText();
        this.created_at_date = twitterInfo.getDate();
    }
    
    // Getters
    public long getStatusId(){
        return this.status_id;
    }
    
    public String getTweetCategory(){
        return this.tweet_category;
    }
    public String getScreenName(){
        return this.screen_name;
    }
    public String getUsername(){
        return this.username;
    }
    public String getTweetText(){
        return this.tweet_text;
    }
    public Date getCreatedAtDate(){
        return this.created_at_date;
    }
    
    // Setters
    public void setTweetCategory(String tweet_category){
        this.tweet_category = tweet_category;
    }
    public void setScreenName(String screen_name){
        this.screen_name = screen_name;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public void setTweetText(String tweet_text){
        this.tweet_text = tweet_text;
    }
    public void setCreatedAtDate(Date created_at_date){
        this.created_at_date = created_at_date;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (this.status_id ^ (this.status_id >>> 32));
        hash = 41 * hash + Objects.hashCode(this.tweet_category);
        hash = 41 * hash + Objects.hashCode(this.screen_name);
        hash = 41 * hash + Objects.hashCode(this.username);
        hash = 41 * hash + Objects.hashCode(this.tweet_text);
        hash = 41 * hash + Objects.hashCode(this.created_at_date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwitterInfoDB other = (TwitterInfoDB) obj;
        if (this.status_id != other.status_id) {
            return false;
        }
        if (!Objects.equals(this.tweet_category, other.tweet_category)) {
            return false;
        }
        if (!Objects.equals(this.screen_name, other.screen_name)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.tweet_text, other.tweet_text)) {
            return false;
        }
        return Objects.equals(this.created_at_date, other.created_at_date);
    }
    
    
}
