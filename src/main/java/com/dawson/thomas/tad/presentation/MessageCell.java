package com.dawson.thomas.tad.presentation;

import com.dawson.thomas.tad.business.TwitterEngine;
import com.dawson.thomas.tad.data.MessageBean;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Represents a single message shown on a List View.
 * 
 * @author Thomas
 */
public class MessageCell extends ListCell<MessageBean> {

    private final static Logger LOG = LoggerFactory.getLogger(MessageCell.class);

    @Override
    protected void updateItem(MessageBean item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(getMessageCell(item));
        }
    }

    /**
     * Used to create a node for displaying a message on a List View.
     * 
     * @param message the message bean containing the message to be displayed.
     * @return a node representing a direct message
     */
    private Node getMessageCell(MessageBean message) {
        HBox node = new HBox();
        node.setPrefWidth(USE_PREF_SIZE);
        Label label = new Label(message.getText());
        label.setFont(Font.font ("System", 17));
        label.setWrapText(true);
        label.setPadding(new Insets(10, 10, 10, 10));
        try {
            TwitterEngine twitter = new TwitterEngine();
            long myId = twitter.getTwitterInstance().getId();
            // If I sent it
            if(message.getSender() == myId){
                node.setAlignment(Pos.CENTER_RIGHT);
                label.setStyle("-fx-background-color: lightblue;");
                label.textFillProperty().set(Color.rgb(255,255,255));
            } else{
                // If I recevied it
                node.setAlignment(Pos.CENTER_LEFT);
            }
        } catch (TwitterException | IllegalStateException ex) {
            LOG.error("Couldn't load message", ex);
        }
        node.getChildren().add(label);
        return node;
    }
}
