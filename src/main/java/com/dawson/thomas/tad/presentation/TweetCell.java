package com.dawson.thomas.tad.presentation;

import com.dawson.thomas.tad.data.TwitterInfo;
import com.dawson.thomas.tad.fxcontrollers.TwitterCellController;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a single tweet shown on a List View.
 * 
 * @author Thomas
 */
public class TweetCell extends ListCell<TwitterInfo> {

    private final static Logger LOG = LoggerFactory.getLogger(TweetCell.class);

    @Override
    protected void updateItem(TwitterInfo item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(getTweetCell(item));
        }
    }

    /**
     * Used to create a node for displaying a tweet on a List View.
     * 
     * @param twitterInfoDB the data bean containing the tweet to be displayed.
     * @return a node representing a tweet
     */
    private Node getTweetCell(TwitterInfo twitterInfo) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/TweetCell.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));

        Node node;
        try {
            node = loader.load();
            TwitterCellController controller = loader.getController();
            controller.setTwitterInfo(twitterInfo);
        } catch (IOException e) {
            LOG.error("Couldn't load fxml file TweetCell.fxml", e);
            // If the fxml is somehow missing, it returns and empty HBox
            node = new HBox();
        }

        return node;
    }
}
