package com.dawson.thomas.tad.presentation;

import com.dawson.thomas.tad.data.TwitterInfoDB;
import com.dawson.thomas.tad.fxcontrollers.SavedTweetCellController;
import com.dawson.thomas.tad.fxcontrollers.SavedTweetsModuleController;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a single saved tweet shown on a List View.
 * 
 * @author Thomas
 */
public class SavedTweetCell  extends ListCell<TwitterInfoDB>{
    
    private final static Logger LOG = LoggerFactory.getLogger(SavedTweetCell.class);

    private final SavedTweetsModuleController parentController;
    
    public SavedTweetCell(SavedTweetsModuleController parentController){
        this.parentController = parentController;
    }
    
    @Override
    protected void updateItem(TwitterInfoDB item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(getTweetCell(item));
        }
    }

    /**
     * Used to create a node for displaying a saved tweet from the database
     * on a List View.
     * 
     * @param twitterInfoDB the data bean containing the tweet to be displayed.
     * @return a node representing a saved tweet
     */
    private Node getTweetCell(TwitterInfoDB twitterInfoDB) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/SavedTweetCell.fxml"));
        loader.setResources(ResourceBundle.getBundle("MessageBundle", Locale.getDefault()));

        Node node;
        try {
            node = loader.load();
            SavedTweetCellController controller = loader.getController();
            controller.setTwitterInfoDB(twitterInfoDB);
            controller.setParentController(parentController);
        } catch (IOException e) {
            LOG.error("Couldn't load fxml file TweetCell.fxml", e);
            // If the fxml is somehow missing, it returns and empty HBox
            node = new HBox();
        }

        return node;
    }
}
