package com.dawson.thomas.tad.presentation;

import com.dawson.thomas.tad.data.UserBean;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a single user in a list of received messages
 * 
 * @author Thomas
 */
public class UserCell extends ListCell<UserBean> {

    private final static Logger LOG = LoggerFactory.getLogger(UserCell.class);

    @Override
    protected void updateItem(UserBean item, boolean empty) {
        super.updateItem(item, empty);

        LOG.debug("updateItem");

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(getTweetCell(item));
        }
    }

    
    /**
     * Used to create a node for displaying a user's name and their most 
     * recent message on a List View.
     * 
     * @param twitterInfoDB the data bean containing the tweet to be displayed.
     * @return a node representing a user.
     */
    private Node getTweetCell(UserBean user) {
        VBox node = new VBox();
        Text username = new Text(user.getUsername());
        username.setFont(Font.font ("System", 17));
        Text lastMsg = new Text(user.getLastMessage());
        lastMsg.setFont(Font.font ("System", 14));
        node.getChildren().add(username);
        node.getChildren().add(lastMsg);
        return node;
    }
}
