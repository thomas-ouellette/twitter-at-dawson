/**
 * The manager package contains utility classes used for 
 * managing application configuration properties (i.e. loading and saving them)
 * 
 * <p>
 * The package currently contains two classes used for 
 * managing the properties: one for the Twitter API properties 
 * and one for the database properties.
 */
package com.dawson.thomas.tad.manager;

import com.dawson.thomas.tad.data.TwitterAPIProperties;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import static java.nio.file.Paths.get;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;

/**
 * Utility class used for saving and loading the properties 
 * needed by the Twitter API. 
 * 
 * @author Thomas
 */
public class TwitterAPIPropertiesManager {
    
    /**
     * Retrieves from a properties file the API keys necessary to 
     * use the twitter API
     *
     * @param apiKeys
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final TwitterAPIProperties apiKeys, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            apiKeys.setConsumerKey(prop.getProperty("oauth.consumerKey"));
            apiKeys.setConsumerSecret(prop.getProperty("oauth.consumerSecret"));
            apiKeys.setAccessToken(prop.getProperty("oauth.accessToken"));
            apiKeys.setAccessTokenSecret(prop.getProperty("oauth.accessTokenSecret"));

            found = true;
        }
        return found;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param apiKeys The bean to store into the properties
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final TwitterAPIProperties apiKeys) throws IOException {

        Properties prop = new Properties();

        prop.setProperty("oauth.consumerKey", apiKeys.getConsumerKey());
        prop.setProperty("oauth.consumerSecret", apiKeys.getConsumerSecret());
        prop.setProperty("oauth.accessToken", apiKeys.getAccessToken());
        prop.setProperty("oauth.accessTokenSecret", apiKeys.getAccessTokenSecret());

        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try (OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "Twitter API Properties");
        }
    }
}
