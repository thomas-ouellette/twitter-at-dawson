/**
 * The manager package contains utility classes used for 
 * managing application configuration properties (i.e. loading and saving them)
 * 
 * <p>
 * The package currently contains two classes used for 
 * managing the properties: one for the Twitter API properties 
 * and one for the database properties.
 */
package com.dawson.thomas.tad.manager;

import com.dawson.thomas.tad.data.DatabaseProperties;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;

/**
 * Class contains 2 methods - one for saving to and one for retrieving from 
 * disk the credentials necessary for the MySQL used for storing saved tweets.
 * 
 * @author Thomas
 */
public class DatabasePropertiesManager {
    
    /**
     * Retrieves from a properties file the database configuration information
     * necessary to use the database to save tweets
     *
     * @param dbProps
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    public final boolean loadTextProperties(final DatabaseProperties dbProps, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            dbProps.setDbName(prop.getProperty("dbName"));
            dbProps.setDbUsername(prop.getProperty("dbUsername"));
            dbProps.setDbPassword(prop.getProperty("dbPassword"));
            dbProps.setDbURL(prop.getProperty("dbURL"));
            dbProps.setDbPort(prop.getProperty("dbPort"));

            found = true;
        }
        return found;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param dbProps properties data bean
     * @throws IOException
     */
    public final void writeTextProperties(final String path, final String propFileName, final DatabaseProperties dbProps) throws IOException {

        Properties prop = new Properties();

        prop.setProperty("dbName", dbProps.getDbName());
        prop.setProperty("dbUsername", dbProps.getDbUsername());
        prop.setProperty("dbPassword", dbProps.getDbPassword());
        prop.setProperty("dbURL", dbProps.getDbURL());
        prop.setProperty("dbPort", dbProps.getDbPort());

        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try (OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "Database Properties");
        }
    }
}
